module Request
  def Request.get(endpoint, params = {})
    begin
      res = RestClient.get(endpoint, { params: params })
      return JSON.parse(res)
    rescue RestClient::ExceptionWithResponse => e
      return nil
    end
  end

  def Request.post(endpoint, body, headers = {})
    begin
      res = RestClient.post(endpoint, body, headers)
      return JSON.parse(res)
    rescue RestClient::ExceptionWithResponse => e
      return nil
    end
  end
end
