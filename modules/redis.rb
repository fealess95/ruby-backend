module RedisModule
  def RedisModule.set(key, value)
    $redis.set(key, value.to_json)
  end

  def RedisModule.get(key)
    result = $redis.get(key)
    if result.nil?
      return nil
    else
      return JSON.parse(result)
    end
  end

  def RedisModule.compose_get_set(key)
    result = RedisModule.get(key)
    if result.nil?
      result = yield
      RedisModule.set(key, result)
    end
    return result
  end
end
