namespace :message do
  desc 'Messages Tasks'

  task create: :environment do
    puts 'RUNNING rake message:create'

    message =
      Message.create(
        {
          "title": 'ruby-backend/bin/bundle exec',
          "content": 'Mã này dành tặng',
          "expected_number_of_users": '1',
          "is_schedule": true,
          "schedule_date": Time.now,
          "kind": 1,
          "status": 1,
          "voucher_id": 1,
        },
      )
    puts "CREATED message: #{message.inspect}"
  end
end
