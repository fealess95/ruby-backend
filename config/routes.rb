Rails
  .application
  .routes
  .draw do
    resources :users
    resources :messages, param: :title do
      get 'broadcasts' => 'broadcasts#index', as: :broadcasts
    end
    resources :vouchers
    resources :broadcasts, param: :id do
      get 'received' => 'broadcasts#received', as: :broadcasts
    end
    resources :micro
    post 'vouchers/batch_get'
    get 'users/get_cache'
end
