require_relative 'environment'

set :environment, Rails.env
set :output, '~/Desktop/Code/learn/ruby-backend/log/cron_log.log'
env :PATH, ENV['PATH']
env :GEM_PATH, ENV['GEM_PATH']
env :GEM_HOME, ENV['GEM_HOME']
set :bundle_command, '~/Desktop/Code/learn/ruby-backend/bin/bundle exec'
set :job_template, nil

# Learn more: http://github.com/javan/whenever

every 1.minutes do
  puts "Crontab is run on #{@environment}"
  rake 'message:create'
end
