# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_09_18_043528) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "broadcasts", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "message_id", null: false
    t.string "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["message_id"], name: "index_broadcasts_on_message_id"
    t.index ["user_id"], name: "index_broadcasts_on_user_id"
  end

  create_table "dragonary_dragons", force: :cascade do |t|
    t.string "name"
    t.integer "red"
    t.integer "blue"
    t.integer "grey"
    t.integer "breed_count"
    t.string "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "messages", force: :cascade do |t|
    t.string "title", null: false
    t.string "content"
    t.string "expected_number_of_users", null: false
    t.boolean "is_schedule", default: false, null: false
    t.datetime "schedule_date"
    t.integer "kind", null: false
    t.integer "status", null: false
    t.integer "voucher_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "vouchers", force: :cascade do |t|
    t.integer "discount", null: false
    t.boolean "is_percent", default: false, null: false
    t.integer "max_discount"
    t.integer "min_order", default: 0
    t.integer "status", default: 1
    t.integer "time_status", default: 1
    t.integer "kind", default: 1
    t.datetime "start_date"
    t.datetime "end_date"
  end

  add_foreign_key "broadcasts", "messages"
  add_foreign_key "broadcasts", "users"
end
