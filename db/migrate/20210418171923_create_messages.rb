class CreateMessages < ActiveRecord::Migration[6.1]
  def change
    create_table :messages do |t|
      t.string :title, null: false
      t.string :content
      t.string :expected_number_of_users, null: false
      t.boolean :is_schedule, default: false, null: false
      t.datetime :schedule_date
      t.integer :kind, null: false
      t.integer :status, null: false
      t.integer :voucher_id
    end
  end
end
