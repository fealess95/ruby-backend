class CreateDragonaryDragons < ActiveRecord::Migration[6.1]
  def change
    create_table :dragonary_dragons do |t|
      t.string :name
      t.integer :red
      t.integer :blue
      t.integer :grey
      t.integer :breed_count
      t.string :note

      t.timestamps
    end
  end
end
