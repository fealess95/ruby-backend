class CreateVouchers < ActiveRecord::Migration[6.1]
  def change
    create_table :vouchers do |t|
      t.integer :discount, null: false
      t.boolean :is_percent, null: false, default: false
      t.integer :max_discount
      t.integer :min_order, default: 0
      t.integer :status, default: 1
      t.integer :time_status, default: 1
      t.integer :kind, default: 1
      t.datetime :start_date
      t.datetime :end_date
    end
  end
end
