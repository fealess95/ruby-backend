module Dragonary
  class Breeding
    class << self
      def find_all_stats(new_dragons = [])
        sum_stats = []
        dragons = DragonaryDragon.all
        if new_dragons
          dragons = dragons + new_dragons
        end
        dragons.each_with_index do |dragon1 , index|
          with_dragons = dragons.slice(index + 1, 2000)
          with_dragons.each do |dragon2|
            red = outcome_stat(dragon1.red, dragon2.red)
            blue = outcome_stat(dragon1.blue, dragon2.blue)
            grey = outcome_stat(dragon1.grey, dragon2.grey)
            sum_stats << {
              :outcome => red + blue + grey,
              :outcome_detail => [red, blue, grey],
              :dragon1 => dragon1,
              :dragon2 => dragon2,
            }
          end
        end
        sum_stats.sort {|a,b| b[:outcome] <=> a[:outcome]}
      end

      # 107 Dragonary::Breeding.find_all_stats_with_new([[20, 22, 23],[22, 24, 25], [23, 25, 26],[23, 25, 26], [28, 30, 31],[28, 30, 31]])
      # 89 Dragonary::Breeding.find_all_stats_with_new([[20, 22, 23],[22, 24, 25], [23, 25, 26],[23, 25, 26]])
      # 74 Dragonary::Breeding.find_all_stats_with_new([[20, 22, 23],[22, 24, 25]])
      def find_all_stats_with_new(array_stats)
        new_dragons = []
        array_stats.each do |stats|
          new_dragons << DragonaryDragon.new(red: stats[0], blue: stats[1], grey: stats[2])
        end
        find_all_stats(new_dragons)
      end

      def outcome_stat(stat1, stat2)
        avg = (stat1 + stat2) / 2
        outcome =  avg * 1.2
        outcome.round
      end
    end
  end
end
