class Broadcast < ApplicationRecord
  belongs_to :user
  belongs_to :message

  scope :filter_by_message_id, ->(id){where(message_id: id)}
  scope :filter_by_user_id, ->(id){where(user_id: id)}

end
