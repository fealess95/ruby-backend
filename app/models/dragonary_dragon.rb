class DragonaryDragon < ApplicationRecord
  validates :red, :blue, :grey, presence: true
end
