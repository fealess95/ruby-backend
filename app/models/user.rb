class User < ApplicationRecord
  has_many :broadcasts

  def broadcasts_received(broadcast)
    broadcasts.filter_by_message_id(broadcast.message_id)
  end
end
