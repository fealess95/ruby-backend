class Message < ApplicationRecord
  belongs_to :voucher, optional: true
  has_many :broadcasts

  scope :find_by_title, ->(title){where(title: title).limit(1).first}

end
