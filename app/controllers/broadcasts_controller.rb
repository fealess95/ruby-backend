class BroadcastsController < ApplicationController
  before_action :set_broadcast, only: %i[show update destroy received]
  before_action :authenticate_user
  before_action :find_message!, only: [:index]

  def index
    if params[:message_title]
      @broadcasts = @message.broadcasts
      render json: @broadcasts and return
    end

    @broadcasts = Broadcast.all.includes(:user, :message)
    @broadcasts = @broadcasts.filter_by_message_id(params[:message_id]) if params[:message_id].present?
    @broadcasts = @broadcasts.filter_by_user_id(params[:user_id]) if params[:user_id].present?
    @broadcasts = @broadcasts.filter_by_user_id(@current_user_id) if params[:auth].present? && !params[:user_id].present?

    render json: @broadcasts
  end

  def received
    @broadcast = current_user.broadcasts_received(@broadcast)

    render json: @broadcast
  end

  def show
    render json: @broadcast
  end

  def create
    @broadcast = Broadcast.new(broadcast_params)

    if @broadcast.save
      render json: @broadcast, status: :created, location: @broadcast
    else
      render json: @broadcast.errors, status: :unprocessable_entity
    end
  end

  def update
    if @broadcast.update(broadcast_params)
      render json: @broadcast
    else
      render json: @broadcast.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @broadcast.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_broadcast
    @broadcast = Broadcast.find(params[:id] || params[:broadcast_id])
  end

  # Only allow a list of trusted parameters through.
  def broadcast_params
    params
      .require(:broadcast)
      .permit(
        :status,
        :user_id,
        :message_id
      )
  end

  def find_message!
    @message = Message.find_by_title(params[:message_title])
  end
end
