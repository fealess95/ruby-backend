class ApplicationController < ActionController::API
  def authenticate_user
    @current_user_id = 2
  end

  def current_user
    @current_user ||= User.find(@current_user_id)
  end
end
