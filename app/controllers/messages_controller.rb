require_relative '../../lib/const.rb'

class MessagesController < ApplicationController
  before_action :set_message, only: %i[show update destroy]

  def index
    @messages =
      Message
        .all
        .select(
          'messages.*',
          'vouchers.discount as voucher_discount',
          'vouchers.max_discount as voucher_max_discount',
          'vouchers.min_order as voucher_min_order'
        )
        .left_joins(:voucher)

    @messages = @messages.where(status: params[:status]) if params[:status]

    @messages = @messages.where(kind: params[:kind]) if params[:kind]

    if params[:start_date] && params[:end_date]
      @messages =
        @messages
          .where('schedule_date <= ?', params[:start_date])
          .where('schedule_date >= ?', params[:end_date])
    end

    if params[:pageSize]
      @messages = @messages.limit(params[:pageSize])
    else
      @messages = @messages.limit(DEFAULT_PAGE_SIZE)
    end

    if params[:page]
      @messages =
        @messages.offset(
          (Integer(params[:page]) - 1) *
            Integer(params[:pageSize] || DEFAULT_PAGE_SIZE)
        )
    end

    render json: @messages
  end

  def show
    render json: @message
  end

  def create
    @message = Message.new(message_params)

    if @message.save
      render json: @message, status: :created, location: @message
    else
      render json: @message.errors, status: :unprocessable_entity
    end
  end

  def update
    if @message.update(message_params)
      render json: @message
    else
      render json: @message.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @message.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_message
    @message = Message.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def message_params
    params
      .require(:message)
      .permit(
        :id,
        :title,
        :content,
        :expected_number_of_users,
        :is_schedule,
        :schedule_date,
        :kind,
        :status,
        :voucher_id
      )
  end
end
