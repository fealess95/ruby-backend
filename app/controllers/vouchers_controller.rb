class VouchersController < ApplicationController
  before_action :set_voucher, only: %i[show update destroy]

  def index
    @vouchers = Voucher.all

    render json: @vouchers
  end

  def batch_get
    if params[:ids].length == 0
      render json: [] and return
    else
      condition = ''

      params[:ids].each_with_index do |id, index|
        if index == 0
          condition += "id = #{id}"
        else
          condition += " OR id = #{id}"
        end
      end

      @vouchers = Voucher.all.where(condition)
    end

    render json: @vouchers
  end

  def show
    render json: @voucher
  end

  def create
    @voucher = Voucher.new(voucher_params)

    if @voucher.save
      render json: @voucher, status: :created, location: @voucher
    else
      render json: @voucher.errors, status: :unprocessable_entity
    end
  end

  def update
    if @voucher.update(voucher_params)
      render json: @voucher
    else
      render json: @voucher.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @voucher.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_voucher
    @voucher = Voucher.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def voucher_params
    params
      .require(:voucher)
      .permit(
        :discount,
        :is_percent,
        :max_discount,
        :min_order,
        :status,
        :time_status,
        :kind,
        :start_date,
        :end_date
      )
  end
end
