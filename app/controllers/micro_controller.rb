require_relative '../../lib/const.rb'
require_relative '../../modules/request.rb'
require_relative '../../modules/redis.rb'
require 'rest-client'

class MicroController < ApplicationController
  include Request
  include RedisModule

  def index
    @messages = Message.all

    @messages = @messages.where(status: params[:status]) if params[:status]

    @messages = @messages.where(kind: params[:kind]) if params[:kind]

    if params[:start_date] && params[:end_date]
      @messages =
        @messages
          .where('schedule_date <= ?', params[:start_date])
          .where('schedule_date >= ?', params[:end_date])
    end

    if params[:pageSize]
      @messages = @messages.limit(params[:pageSize])
    else
      @messages = @messages.limit(DEFAULT_PAGE_SIZE)
    end

    if params[:page]
      @messages =
        @messages.offset(
          (Integer(params[:page]) - 1) *
            Integer(params[:pageSize] || DEFAULT_PAGE_SIZE)
        )
    end

    @messages = many_time_get_vouchers

    # @messages = one_time_get_vouchers

    render json: @messages
  end

  def get_voucher_by_id(id)
    Request.get("#{$api_vouchers_domain}vouchers/#{id}")
  end

  def many_time_get_vouchers
    @messages.as_json.each do |item|
      if !item['voucher_id'].nil?
        r_key = "voucher_#{item['voucher_id']}"
        voucher =
          RedisModule.compose_get_set(r_key) do
            get_voucher_by_id(item['voucher_id'])
          end
        item['voucher'] = voucher
      else
        item['voucher'] = nil
      end
    end
  end

  def one_time_get_vouchers
    voucher_ids = []

    @messages.each do |item|
      voucher_ids.push(item.voucher_id) unless item.voucher_id.nil?
    end

    @vouchers =
      Request.post(
        "#{$api_vouchers_domain}vouchers/batch_get",
        { ids: voucher_ids }
      ) || []

    @messages = @messages.as_json
    @messages.each do |message|
      message['voucher'] =
        @vouchers.find { |voucher| voucher['id'] == message['voucher_id'] }
    end

    @messages
  end
end
