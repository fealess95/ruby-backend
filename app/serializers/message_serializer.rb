class UserSerializer < ActiveModel::Serializer
  attributes :id,
             :title,
             :content,
             :expected_number_of_users,
             :is_schedule,
             :schedule_date,
             :kind,
             :status,
             :voucher_id
end
