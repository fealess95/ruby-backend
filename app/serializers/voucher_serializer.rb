class VoucherSerializer < ActiveModel::Serializer
  attributes :id,
             :discount,
             :is_percent,
             :max_discount,
             :min_order,
             :status,
             :time_status,
             :kind,
             :start_date,
             :end_date
end
